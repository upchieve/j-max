package org.upchieve.architecture.personas

import com.structurizr.model.Model
import com.structurizr.model.Person

class Student(model: Model) {

    val user: Person = model.addPerson("Student", "A student who has qualified for the platform looking for academic assistance.")

}
