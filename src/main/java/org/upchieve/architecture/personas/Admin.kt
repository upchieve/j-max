package org.upchieve.architecture.personas

import com.structurizr.model.Model
import com.structurizr.model.Person

class Admin(model: Model) {

    val user: Person = model.addPerson("Admin", "An UPchieve staff member who does administrative tasks on the platform.")

}
