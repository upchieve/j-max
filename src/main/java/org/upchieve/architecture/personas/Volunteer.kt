package org.upchieve.architecture.personas

import com.structurizr.model.Model
import com.structurizr.model.Person

class Volunteer(model: Model) {

    val user: Person = model.addPerson("Volunteer Tutor", "A user who provides tutoring to students through the platform.")

}
