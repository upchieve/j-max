package org.upchieve.architecture.systems

import com.structurizr.model.Model
import com.structurizr.model.SoftwareSystem

class SendGrid(model: Model) {

    val system: SoftwareSystem = model.addSoftwareSystem("SendGrid", "Email-by-api")
}
