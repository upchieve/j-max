package org.upchieve.architecture.systems

import org.upchieve.architecture.personas.Admin
import org.upchieve.architecture.personas.Student
import org.upchieve.architecture.personas.Volunteer
import org.upchieve.architecture.styles.GTags
import cc.catalysts.structurizr.kotlin.addContainer
import com.structurizr.model.Container
import com.structurizr.model.InteractionStyle
import com.structurizr.model.Model
import com.structurizr.model.SoftwareSystem
import com.structurizr.view.PaperSize
import com.structurizr.view.ViewSet

class Frontend(model: Model) {

    val system: SoftwareSystem = model.addSoftwareSystem("Frontend", "API server, database, cache and storage.")

    private var cdnBucket: Container = system.addContainer("CDN Bucket", "File storage", "Azure Storage") { withTags(GTags.Bucket) }
    private var staticSite: Container = system.addContainer("Website", "Main frontend application", "Vue.js") { withTags(GTags.Web) }

    fun createUsePaths(admin: Admin, volunteer: Volunteer, student: Student, cloudflare: Cloudflare, newRelic: NewRelic, googleAnalytics: GoogleAnalytics, posthog: Posthog, unleash: Unleash) {
        system.uses(cloudflare.system, "Sends api requests", "https", InteractionStyle.Synchronous)
        staticSite.uses(cdnBucket, "Retrieves files like icons and Zwibbler", "https", InteractionStyle.Synchronous)
        staticSite.uses(cloudflare.system, "Sends api requests", "https", InteractionStyle.Synchronous)
        staticSite.uses(newRelic.system, "Sends browser metric data", "NewRelic SDK", InteractionStyle.Asynchronous)
        admin.user.uses(staticSite, "Accesses in browser", "Browser")
        student.user.uses(staticSite, "Accesses in browser", "Browser")
        volunteer.user.uses(staticSite, "Accesses in browser", "Browser")
        staticSite.uses(googleAnalytics.system, "Sending analytics data", "Google Analytics SDK", InteractionStyle.Asynchronous)
        staticSite.uses(posthog.system, "Sending analytics data", "Posthog SDK", InteractionStyle.Asynchronous)
        staticSite.uses(unleash.system, "Retrieving feature flag data", "Unleash SDK", InteractionStyle.Synchronous)
    }

    fun createView(views: ViewSet, admin: Admin, volunteer: Volunteer, student: Student, cloudflare: Cloudflare) {
        val frontendContainerView = views.createContainerView(system, "Frontend Containers", "Drill down into the containers that make up the backend system.")
        frontendContainerView.paperSize = PaperSize.Slide_4_3
        frontendContainerView.add(cdnBucket)
        frontendContainerView.add(staticSite)
        frontendContainerView.add(admin.user)
        frontendContainerView.add(volunteer.user)
        frontendContainerView.add(student.user)
        frontendContainerView.add(cloudflare.system)
    }

}
