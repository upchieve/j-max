package org.upchieve.architecture.systems

import com.structurizr.model.Model
import com.structurizr.model.SoftwareSystem

class GoogleAnalytics(model: Model) {
    val system: SoftwareSystem = model.addSoftwareSystem("Google Analytics", "User/advertising analytics")
}
