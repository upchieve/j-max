package org.upchieve.architecture.systems

import org.upchieve.architecture.systems.backend.Backend
import com.structurizr.model.InteractionStyle
import com.structurizr.model.Model
import com.structurizr.model.SoftwareSystem

class Ambassador(model: Model) {

    val system: SoftwareSystem = model.addSoftwareSystem("Ambassador", "API gateway, rate limiting")

    fun createUsePaths(backend: Backend) {
        system.uses(backend.system, "Sends api requests", "https", InteractionStyle.Synchronous)
    }
}
