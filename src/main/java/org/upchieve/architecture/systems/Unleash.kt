package org.upchieve.architecture.systems

import com.structurizr.model.Model
import com.structurizr.model.SoftwareSystem

class Unleash(model: Model) {
    val system: SoftwareSystem = model.addSoftwareSystem("Unleash", "Feature flags")
}
