package org.upchieve.architecture.systems

import com.structurizr.model.Model
import com.structurizr.model.SoftwareSystem

class NewRelic(model: Model) {
    val system: SoftwareSystem = model.addSoftwareSystem("NewRelic", "APM, Infrastructure and Browser Metrics")

}
