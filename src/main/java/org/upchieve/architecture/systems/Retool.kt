package org.upchieve.architecture.systems

import com.structurizr.model.Model
import com.structurizr.model.SoftwareSystem

class Retool(model: Model) {
    val system: SoftwareSystem = model.addSoftwareSystem("Retool", "Internal business applications")
}
