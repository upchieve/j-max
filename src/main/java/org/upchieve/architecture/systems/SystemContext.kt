package org.upchieve.architecture.systems

import com.structurizr.model.InteractionStyle
import org.upchieve.architecture.personas.Admin
import org.upchieve.architecture.systems.backend.Backend
import com.structurizr.view.PaperSize
import com.structurizr.view.ViewSet
import org.upchieve.architecture.personas.Student
import org.upchieve.architecture.personas.Volunteer

class SystemContext {

    fun createUsePaths(admin: Admin, volunteer: Volunteer, student: Student, frontend: Frontend, cloudflare: Cloudflare, ambassador: Ambassador, backend: Backend, newRelic: NewRelic, unleash: Unleash, posthog: Posthog, retool: Retool, googleAnalytics: GoogleAnalytics) {
        admin.user.uses(posthog.system, "Reviewing user analytics", "Browser")
        admin.user.uses(retool.system, "Administrative functions and reporting", "Browser")
        admin.user.uses(googleAnalytics.system, "Reviewing marketing metrics", "Browser")
        admin.user.uses(frontend.system, "Adminstrative functions and reporting", "Browser")
        volunteer.user.uses(frontend.system, "Providing tutoring", "Browser")
        student.user.uses(frontend.system, "Receiving tutoring", "Browser")
        frontend.system.uses(cloudflare.system, "Send api requests", "REST", InteractionStyle.Synchronous)
        frontend.system.uses(newRelic.system, "Send browser metrics", "NewRelic SDK", InteractionStyle.Asynchronous)
        frontend.system.uses(unleash.system, "Retrieves feature flag data", "Unleash SDK", InteractionStyle.Synchronous)
        frontend.system.uses(posthog.system, "Sends user analytics data", "Posthog SDK", InteractionStyle.Asynchronous)
        frontend.system.uses(googleAnalytics.system, "Sends user analytics data", "Google Analytics SDK", InteractionStyle.Asynchronous)
        cloudflare.system.uses(ambassador.system, "Send api requests", "REST", InteractionStyle.Synchronous)
        ambassador.system.uses(backend.system, "Send api requests", "REST", InteractionStyle.Synchronous)
        backend.system.uses(newRelic.system, "Sends APM metrics", "NewRelic SDK", InteractionStyle.Asynchronous)
        backend.system.uses(unleash.system, "Retrieves feature flag data", "Unleash SDK", InteractionStyle.Synchronous)
        backend.system.uses(posthog.system, "Sends user analytics data", "Posthog SDK", InteractionStyle.Synchronous)
        retool.system.uses(backend.system, "Retrieves data from API and database", "Rest/Mongo", InteractionStyle.Synchronous)
    }

    fun createView(views: ViewSet, backend: Backend) {
        val contextView = views.createSystemContextView(backend.system, "UPchieve System Context", "Overview of how users interact with UPchieve systems")
        contextView.paperSize = PaperSize.Slide_4_3
        contextView.addAllSoftwareSystems()
        contextView.addAllPeople()
    }
}
