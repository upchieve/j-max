package org.upchieve.architecture.systems

import com.structurizr.model.InteractionStyle
import com.structurizr.model.Model
import com.structurizr.model.SoftwareSystem

class Cloudflare(model: Model) {

    val system: SoftwareSystem = model.addSoftwareSystem("Cloudflare", "CDN, firewall, DDOS protection")

    fun createUsePaths(ambassador: Ambassador) {
        system.uses(ambassador.system, "Sends api requests", "https", InteractionStyle.Synchronous)
    }

}
