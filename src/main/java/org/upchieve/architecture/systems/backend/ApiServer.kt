package org.upchieve.architecture.systems.backend

import com.structurizr.model.Component
import com.structurizr.model.Container
import com.structurizr.model.SoftwareSystem
import com.structurizr.model.InteractionStyle
import com.structurizr.view.PaperSize
import com.structurizr.view.ViewSet
import org.upchieve.architecture.personas.Student
import org.upchieve.architecture.personas.Volunteer

class ApiServer(system: SoftwareSystem) {
    val container: Container = system.addContainer("API", "Main monolithic backend API", "Node/Express/TypeScript")

    private var sessionService: Component = container.addComponent("Session Service", "Service for managing tutoring sessions", "TypeScript")
    private var authService: Component? = container.addComponent("Auth Service", "Service for managing authentication of users", "TypeScript")
    private var userSessionMetricsService: Component = container.addComponent("User Session Metrics Service", "Service for calculating session related metrics for a user", "TypeScript")
    private var feedbackService: Component = container.addComponent("Feedback Service", "Service for managing the feeback submissions for a tuturing session", "TypeScript")

    fun createView(views: ViewSet) {
        val apiServerComponentView = views.createComponentView(container, "API Server Components", "Drill down into the components that make up the API Server.")
        apiServerComponentView.paperSize = PaperSize.Slide_4_3
        apiServerComponentView.add(sessionService)
        apiServerComponentView.add(authService)
        apiServerComponentView.add(userSessionMetricsService)
        apiServerComponentView.add(feedbackService)
    }

    fun createEndSessionEventUsePaths(volunteer: Volunteer) {
        volunteer.user.uses(sessionService, "ends a session", "REST")
        sessionService.uses(sessionService, "sends a session ended event", "", InteractionStyle.Asynchronous)
        sessionService.uses(userSessionMetricsService, "sends a session ended event", "", InteractionStyle.Asynchronous)
        userSessionMetricsService.uses(userSessionMetricsService, "sends a session processors ready event", "", InteractionStyle.Asynchronous)
        userSessionMetricsService.uses(sessionService, "sends a session flags set event", "", InteractionStyle.Asynchronous)
        sessionService.uses(sessionService, "sends a session metrics calculated event", "", InteractionStyle.Asynchronous)
    }

    fun createEndSessionEventView(views: ViewSet, volunteer: Volunteer) {
        val endSessionView = views.createDynamicView(container, "EndSessionEvent", "Events that are triggered once a session has ended")
        
        endSessionView.add(volunteer.user, sessionService);

        endSessionView.startParallelSequence()
        endSessionView.add(sessionService, sessionService);
        endSessionView.endParallelSequence()

        endSessionView.startParallelSequence()
        endSessionView.add(sessionService, userSessionMetricsService);
        endSessionView.endParallelSequence()

        endSessionView.add(userSessionMetricsService, userSessionMetricsService);
        endSessionView.add(userSessionMetricsService, sessionService)
        endSessionView.add(sessionService, "sends a session metrics calculated event", sessionService)

        endSessionView.paperSize = PaperSize.Slide_4_3
    }

    fun createFeedbackSavedEventUsePaths(volunteer: Volunteer, student: Student) {
        volunteer.user.uses(feedbackService, "submits feedback", "REST")
        student.user.uses(feedbackService, "submits feedback", "REST")
        feedbackService.uses(userSessionMetricsService, "sends a feedback saved event", "", InteractionStyle.Asynchronous)
        userSessionMetricsService.uses(userSessionMetricsService, "sends a feedback processors ready event", "", InteractionStyle.Asynchronous)
    }

    fun createFeedbackSavedEventView(views: ViewSet, volunteer: Volunteer, student: Student) {
        val feedbackSavedView = views.createDynamicView(container, "FeedbackSavedEventView", "Events that are triggered once feedback has been submitted")
        
        feedbackSavedView.add(volunteer.user, feedbackService)
        feedbackSavedView.add(student.user, feedbackService)
        feedbackSavedView.add(feedbackService, userSessionMetricsService)
        feedbackSavedView.add(userSessionMetricsService, userSessionMetricsService)

        feedbackSavedView.paperSize = PaperSize.Slide_4_3
    }
}
