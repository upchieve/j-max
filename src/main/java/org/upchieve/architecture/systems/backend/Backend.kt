package org.upchieve.architecture.systems.backend

import org.upchieve.architecture.styles.GTags
import org.upchieve.architecture.systems.Ambassador
import org.upchieve.architecture.systems.NewRelic
import org.upchieve.architecture.systems.Posthog
import org.upchieve.architecture.systems.Unleash
import cc.catalysts.structurizr.kotlin.addContainer
import com.structurizr.model.Container
import com.structurizr.model.InteractionStyle
import com.structurizr.model.Model
import com.structurizr.model.SoftwareSystem
import com.structurizr.view.PaperSize
import com.structurizr.view.ViewSet

class Backend(model: Model) {

    val system: SoftwareSystem = model.addSoftwareSystem("Backend", "API server, database, cache and storage.")
    val apiServer: ApiServer = ApiServer(system)

    private var cache: Container = system.addContainer("Cache", "Job queue and temporary cache", "Redis") { withTags(GTags.Database) }
    private var database: Container = system.addContainer("Database", "Stores all application working data", "MongoDB") { withTags(GTags.Database) }
    private var sessionPhotoBucket: Container = system.addContainer("Session Photos", "Storage for photos uploaded by students", "Amazon S3") { withTags(GTags.Bucket) }
    private var whiteboardBucket: Container = system.addContainer("Whiteboard Docs", "Storage for zwibbler documents", "Azure Storage") { withTags(GTags.Bucket) }
    private var photoIdBucket:Container = system.addContainer("ID Photos", "Storage for volunteer photo ids", "Amazon S3") { withTags(GTags.Bucket) }
    private var worker:Container = system.addContainer("Worker", "Processes job queues", "Node/Bull/TypeScript")

    fun createUsePaths(newRelic: NewRelic, posthog: Posthog, unleash: Unleash, ambassador: Ambassador) {
        apiServer.container.uses(cache, "Creates jobs, temporarily stores data", "ioredis", InteractionStyle.Asynchronous)
        worker.uses(database, "Manipulates and reads data", "Mongoose", InteractionStyle.Synchronous)
        worker.uses(cache, "Processes jobs", "ioredis", InteractionStyle.Asynchronous)
        apiServer.container.uses(database, "Stores data", "Mongoose", InteractionStyle.Synchronous)
        apiServer.container.uses(sessionPhotoBucket, "Photo storage", "Amazon SDK", InteractionStyle.Synchronous)
        apiServer.container.uses(photoIdBucket, "Photo storage", "Amazon SDK", InteractionStyle.Synchronous)
        apiServer.container.uses(whiteboardBucket, "Whiteboard state storage", "Azure SDK", InteractionStyle.Synchronous)
        apiServer.container.uses(newRelic.system, "Sends APM", "NewRelic SDK", InteractionStyle.Asynchronous)
        apiServer.container.uses(posthog.system, "Sends user events", "Posthog SDK", InteractionStyle.Synchronous)
        apiServer.container.uses(unleash.system, "Retrieves feature flag data", "Unleash SDK", InteractionStyle.Synchronous)
        worker.uses(newRelic.system, "Sends APM", "NewRelic SDK", InteractionStyle.Asynchronous)
        ambassador.system.uses(apiServer.container, "Sends api requests", "REST", InteractionStyle.Synchronous)
    }

    fun createView(views: ViewSet, ambassador: Ambassador, unleash: Unleash, posthog: Posthog) {
        val backendContainerView = views.createContainerView(system, "Backend Containers", "Drill down into the containers that make up the backend system.")
        backendContainerView.paperSize = PaperSize.Slide_4_3
        backendContainerView.add(cache)
        backendContainerView.add(database)
        backendContainerView.add(apiServer.container)
        backendContainerView.add(sessionPhotoBucket)
        backendContainerView.add(whiteboardBucket)
        backendContainerView.add(photoIdBucket)
        backendContainerView.add(worker)
        backendContainerView.add(ambassador.system)
        backendContainerView.add(unleash.system)
        backendContainerView.add(posthog.system)
    }
}
