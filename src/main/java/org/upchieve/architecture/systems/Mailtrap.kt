package org.upchieve.architecture.systems

import com.structurizr.model.Model
import com.structurizr.model.SoftwareSystem

class Mailtrap(model: Model) {

    val system: SoftwareSystem = model.addSoftwareSystem("Mailtrap", "Email testing")
}
