package org.upchieve.architecture.systems

import com.structurizr.model.Model
import com.structurizr.model.SoftwareSystem

class Posthog(model: Model) {
    val system: SoftwareSystem = model.addSoftwareSystem("Posthog", "User analytics")
}
