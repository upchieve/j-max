package org.upchieve.architecture.styles

import com.structurizr.Workspace
import com.structurizr.model.Tags
import com.structurizr.view.Shape
import com.structurizr.view.Styles

class StyleProvider(workspace: Workspace) {

    private val styles: Styles = workspace.views.configuration.styles

    fun addStyles() {
        styles.addElementStyle(GTags.Database).shape(Shape.Cylinder).background(Colors.BlondeOnBlonde)
        styles.addElementStyle(GTags.MobileApp).shape(Shape.MobileDevicePortrait).background(Colors.SeriesB)
        styles.addElementStyle(GTags.Web).shape(Shape.WebBrowser).background(Colors.SeriesB)
        styles.addElementStyle(GTags.Bucket).shape(Shape.Folder).background(Colors.BlondeOnBlonde)
        styles.addElementStyle(GTags.Events).shape(Shape.Pipe).background(Colors.DangerZone)
        styles.addElementStyle(GTags.Vendor).shape(Shape.Hexagon).background(Colors.GrilledCheese).color(Colors.PowderDay)
        styles.addElementStyle(Tags.SOFTWARE_SYSTEM).background(Colors.Rio).color(Colors.PowderDay)
        styles.addElementStyle(Tags.PERSON).background(Colors.Ninja).color(Colors.PowderDay).shape(Shape.Person)
        styles.addElementStyle(Tags.CONTAINER).background(Colors.Maverick).color(Colors.PowderDay)
        styles.addElementStyle(Tags.COMPONENT).background(Colors.TenEighty).color(Colors.PowderDay).shape(Shape.RoundedBox)
        styles.addRelationshipStyle(Tags.ASYNCHRONOUS).dashed(true)
        styles.addRelationshipStyle(Tags.SYNCHRONOUS).dashed(false)
    }
}

object GTags {
    const val Web: String = "Web"
    const val MobileApp: String = "MobileApp"
    const val Database: String = "Database"
    const val Bucket: String = "Bucket"
    const val Events: String = "Events"
    const val Vendor: String = "Vendor"
}

object Colors {
    // Primary brand colors
    const val Ninja: String = "#091325" // almost black
    const val Maverick: String = "#3D94F0" // light blue
    const val GrilledCheese: String = "#F7971D" // orange
    const val PowderDay: String = "#FFFFFF" // white

    // Secondary brand colors
    const val DangerZone: String = "#E85C3A" // red
    const val Rio: String = "#1948B8" // darker blue
    const val BlondeOnBlonde: String = "#EFB126" // yellow
    const val SeriesB: String = "#3FD181" // green
    const val TenEighty: String = "#2B0333" // also almost black. but purplish.
}
