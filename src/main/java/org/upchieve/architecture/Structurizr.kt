package org.upchieve.architecture
import org.upchieve.architecture.personas.Admin
import org.upchieve.architecture.personas.Volunteer
import org.upchieve.architecture.personas.Student
import org.upchieve.architecture.styles.StyleProvider
import org.upchieve.architecture.systems.*
import org.upchieve.architecture.systems.backend.Backend
import com.structurizr.Workspace
import com.structurizr.api.StructurizrClient
import org.upchieve.architecture.systems.backend.ApiServer

class Structurizr {

    companion object {
        private val WORKSPACE_ID: Long = System.getenv("STRUCTURIZR_WORKSPACE").toLong()
        private val API_KEY = System.getenv("STRUCTURIZR_KEY")
        private val API_SECRET = System.getenv("STRUCTURIZR_SECRET")

        @Throws(Exception::class)
        @JvmStatic
        fun main(args: Array<String>) {
            // a Structurizr workspace is the wrapper for a software org.upchieve.architecture model, views and documentation
            var workspace = Workspace("Dev UPchieve Workspace", "Dev testing workspace to test and validate diagrams")
            if (System.getenv("TARGET_ENV") == "prod") {
                workspace = Workspace("UPchieve Architecture", "Canonical representation of UPchieve systems, applications and relationships")
            }
    
            val model = workspace.model
            val views = workspace.views

            // Users
            val student = Student(model)
            val admin = Admin(model)
            val volunteer = Volunteer(model)

            // Software Systems
            val cloudflare = Cloudflare(model)
            val ambassador = Ambassador(model)
            val newRelic = NewRelic(model)
            val backend = Backend(model)
            val frontend = Frontend(model)
            val posthog = Posthog(model)
            val retool = Retool(model)
            val googleAnalytics = GoogleAnalytics(model)
            val unleash = Unleash(model)

            val systemContext = SystemContext()
            systemContext.createUsePaths(admin, volunteer, student, frontend, cloudflare, ambassador, backend, newRelic, unleash, posthog, retool, googleAnalytics)
            systemContext.createView(views, backend)

            backend.createUsePaths(newRelic, posthog, unleash, ambassador)
            backend.createView(views, ambassador, unleash, posthog)
            backend.apiServer.createView(views)
            backend.apiServer.createEndSessionEventUsePaths(volunteer)
            backend.apiServer.createEndSessionEventView(views, volunteer)
            backend.apiServer.createFeedbackSavedEventUsePaths(volunteer, student)
            backend.apiServer.createFeedbackSavedEventView(views, volunteer, student)

            frontend.createUsePaths(admin, volunteer, student, cloudflare, newRelic, googleAnalytics, posthog, unleash)
            frontend.createView(views, admin, volunteer, student, cloudflare)

            cloudflare.createUsePaths(ambassador)
            ambassador.createUsePaths(backend)

            val styleProvider = StyleProvider(workspace)
            styleProvider.addStyles()

            uploadWorkspaceToStructurizr(workspace)
        }

        @Throws(Exception::class)
        fun uploadWorkspaceToStructurizr(workspace: Workspace) {
            val structurizrClient = StructurizrClient(API_KEY, API_SECRET)
            structurizrClient.putWorkspace(WORKSPACE_ID, workspace)
        }
    }


}
